﻿using UnityEngine;
using System.Collections;
using UnityEngine.Rendering;
using System;

public static class CommandBufferPool
{
    private static ObjectPool<CommandBuffer> bufferPool = new ObjectPool<CommandBuffer>(null, x => x.Clear());

    public static CommandBuffer Get()
    {
        var buffer = bufferPool.Get();
        buffer.name = "Unnamed Commandbuffer";
        return buffer;
    }
    

    public static CommandBuffer Get(string name)
    {
        var buffer = bufferPool.Get();
        buffer.name = name;
        return buffer;
    }

    public static void Release(CommandBuffer buffer)
    {
        bufferPool.Release(buffer);
    }
}
